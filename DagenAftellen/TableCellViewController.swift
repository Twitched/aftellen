//
//  TableCellViewController.swift
//  DagenAftellen
//
//  Created by Arjen Molhoek on 08-03-16.
//  Copyright © 2016 Arjen Molhoek. All rights reserved.
//

import UIKit
import RealmSwift


class TableCellViewController: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dagenLabel: UILabel!
    @IBOutlet weak var emojiLabel: UILabel!
    
}
