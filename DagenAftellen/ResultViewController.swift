//
//  ResultViewController.swift
//  DagenAftellen
//
//  Created by Arjen Molhoek on 08-03-16.
//  Copyright © 2016 Arjen Molhoek. All rights reserved.
//

import UIKit
import RealmSwift
import Social
import Accounts
import MessageUI


extension String {
    
    func contains(find: String) -> Bool{
        return self.rangeOfString(find) != nil
    }
}


class ResultViewController: UIViewController , MFMailComposeViewControllerDelegate {
    
    var days : Results<DagenAftellen>?
    
    @IBOutlet weak var dagenLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var urenLabel: UILabel!
    @IBOutlet weak var minutenLabel: UILabel!
    @IBOutlet weak var secondenLabel: UILabel!
    @IBOutlet weak var daysToGoLabel: UILabel!
    @IBOutlet weak var emojiLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var dagenOfDagLabel: UILabel!
   
    var timer: NSTimer!
    var dagenAftellenInt: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timer =  NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(ResultViewController.update), userInfo: nil, repeats: true)
 
        
        let realm = try! Realm()
        self.days = realm.objects(DagenAftellen)
        let title = days![dagenAftellenInt].title
        let emoji = days![dagenAftellenInt].emojiLabel
        
        
        
        let date = days![dagenAftellenInt].dateReached
        var date2 : String {
            let dateformatter = NSDateFormatter()
            dateformatter.dateFormat = "dd-MM-yyyy"
            return dateformatter.stringFromDate(date)
        }
            dagenLabel.text = date2
        let end = date2
        let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
        let endDate:NSDate = dateFormatter.dateFromString(end)!
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day, .Hour, .Minute, .Second], fromDate: NSDate(), toDate: endDate, options: [])
            titleLabel.text = title
            daysToGoLabel.text = String(components.day)
            urenLabel.text = String(components.hour)
            minutenLabel.text = String(components.minute)
            secondenLabel.text = String(components.second)
            emojiLabel.text = emoji
     
        if daysToGoLabel.text == "1" {
        dagenOfDagLabel.text = "Dag" 
    }
        
        if daysToGoLabel.text! == "0" && urenLabel.text! == "0" && minutenLabel.text! == "0" && secondenLabel.text! == "0" {
        timer.invalidate()
        }
        
 // change backgrounds if certain text is used in the title
        if title.contains("Efteling") || title.contains("efteling") {
            imageView.image = UIImage(named: "efteling.jpg")
        }
        
        if title.contains("vakantie") || title.contains("Vakantie") {
            imageView.image = UIImage(named: "Vakantie.jpg")
        }
        
        if title.contains("Ski") || title.contains("ski") {
            imageView.image = UIImage(named: "Ski.jpg")
        }
        
        if title.contains("Dierentuin") || title.contains("dierentuin") {
            imageView.image = UIImage(named:"leeuw.jpg")
        }
        
// stop timer if all labels reaches 0 
        
        if String(components.day).contains("-") {
            timer.invalidate()
            daysToGoLabel.text = "0"
            urenLabel.text = "0"
            minutenLabel.text = "0"
            secondenLabel.text = "0"

            if String(components.day).contains("-1")  {
                titleLabel.text = "Dit evenement is \(String(components.day)) dag geleden"

            } else {
            
            titleLabel.text = "Dit evenement is \(String(components.day)) dagen geleden"
            }
           
            
        }

        
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(true)
            timer.invalidate()
    }

func update() {
    let realm = try! Realm()
        self.days = realm.objects(DagenAftellen)
    let title = days![dagenAftellenInt].title
    let date = days![dagenAftellenInt].dateReached
    var date2 : String {
        let dateformatter = NSDateFormatter()
       dateformatter.dateFormat = "dd-MM-yyyy"
       return dateformatter.stringFromDate(date)
    }
    let end = date2
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy"
    let endDate:NSDate = dateFormatter.dateFromString(end)!
    let calendar = NSCalendar.currentCalendar()
    let components = calendar.components([.Day, .Hour, .Minute, .Second], fromDate: NSDate(), toDate: endDate, options: [])
        daysToGoLabel.text = String(components.day)
        urenLabel.text = String(components.hour)
        minutenLabel.text = String(components.minute)
        secondenLabel.text = String(components.second)
    var count = Int(components.second)
    if(count > 0){
        secondenLabel.text = String(count--)
        }
}
    
    @IBAction func shareOnFacebookButton(sender: AnyObject) {
    
        
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook) {
            
            let facebookSheet: SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            let string0 = "Over "
            let string1 = self.daysToGoLabel.text
            let string2 = " dagen is het "
             let string3 = self.titleLabel.text
            let space = " "
            let emoji = self.emojiLabel.text
            
            facebookSheet.setInitialText(string0 + string1! + string2 + string3! + space + emoji! )
            facebookSheet.addURL(NSURL(string: "http://www.aftellen.info"))

            self.presentViewController(facebookSheet, animated: true, completion: nil)
            
        } else {
            
            let alert = UIAlertController(title: "Account niet beschikbaar", message: "Log in op facebook via instellingen", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func shareOnTwitterButton(sender: AnyObject) {
    
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter) {
        
            let twitterSheet : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            
            let string0 = "Over "
            let string1 = self.daysToGoLabel.text
            let string2 = " dagen is het "
            let string3 = self.titleLabel.text
            let space = " "
            let emoji = self.emojiLabel.text
            
            twitterSheet.setInitialText(string0 + string1! + string2 + string3! + space + emoji! )
            twitterSheet.addURL(NSURL(string: "http://www.aftellen.info"))
            self.presentViewController(twitterSheet, animated: true, completion: nil)
            
        
        } else {
            let alert = UIAlertController(title: "Account niet beschikbaar", message: "Log in op twitter via instellingen", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            
            self.presentViewController(alert, animated: true, completion: nil)

        }
        
    }
    
    @IBAction func feedBackButton(sender: AnyObject) {
        
       
        let mailComposeViewcontroller = configuredMailComposeViewControlleR()
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewcontroller, animated: true, completion: nil)
        } else {
            showSendEmailAlert()
        }
        
        
    }
    
    func configuredMailComposeViewControlleR() -> MFMailComposeViewController{
    
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        mailComposerVC.setToRecipients(["info@aftellen.info"])
        mailComposerVC.setSubject("Feedback")
        mailComposerVC.setMessageBody("Voor de app Aftellen heb ik een opmerking of idee.   " , isHTML: false)
    
        return mailComposerVC
        
    }
    
    func showSendEmailAlert() {
        let sendMailErrorAlert = UIAlertController(title: "Test", message: "TEST", preferredStyle: .Alert)
        sendMailErrorAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        self.presentViewController(sendMailErrorAlert, animated: true, completion: nil)
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        switch result.rawValue {
            
        case MFMailComposeResultCancelled.rawValue:
            print("cancelled mail")
         
        case MFMailComposeResultSent.rawValue:
            print("Mail sent")
            
            default:
            break
 }
        self.dismissViewControllerAnimated(true, completion: nil)
}

}




