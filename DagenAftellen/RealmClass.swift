//
//  RealmClass.swift
//  DagenAftellen
//
//  Created by Arjen Molhoek on 09-03-16.
//  Copyright © 2016 Arjen Molhoek. All rights reserved.
//

import Foundation
import RealmSwift


class DagenAftellen : Object {
    
    dynamic var title : String!
    dynamic var dateReached : NSDate!
    dynamic var emojiLabel : String!
  
    
}
