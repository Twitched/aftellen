//
//  ViewController.swift
//  DagenAftellen
//
//  Created by Arjen Molhoek on 08-03-16.
//  Copyright © 2016 Arjen Molhoek. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    
    var days : Results<DagenAftellen>?
    
   // let notificationArray = UIApplication.sharedApplication().scheduledLocalNotifications

    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        let allowedOrNot = UIApplication.sharedApplication().currentUserNotificationSettings()
       // print(notificationArray)
     // UIApplication.sharedApplication().cancelAllLocalNotifications()
        
        if allowedOrNot?.types == .None {
            // user did not allow notifications
            let alert = UIAlertController(title: "Notificiaties niet toegestaan", message: "Indien je toch notificaties wilt ontvangen, ga naar instellingen => Aftellen en geef toestemming voor notificaties", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil) )
            self.presentViewController(alert, animated: true, completion: nil)
            
            return
        }
 
    }
    
    override func viewWillAppear(animated: Bool) {
        updateUI()
    }
    
    override func viewDidAppear(animated: Bool) {
        if self.days!.count == 0 {
            alertMessage("Hallo 😄", message: "Klik rechtsboven op het  \"+\" teken om je eerste evenement toe te voegen")
        }

    }
    
    func alertMessage(title:String, message:String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            self.navigationController?.popViewControllerAnimated(true)
            
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
        
    } // end alertMessage
    
    func updateUI(){
        let realm = try! Realm()
        self.days = realm.objects(DagenAftellen)
        self.tableView.reloadData()
       
           }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.days!.count
    
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

//        self.days = self.days!.sorted("dateReached")
//        let day = days![indexPath.row]

        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! TableCellViewController
        let theDay = self.days![indexPath.row]
        var date = theDay.dateReached
        var date2 : String {
            let dateformatter = NSDateFormatter()
            dateformatter.dateFormat = "dd-MM-yyyy"
        return dateformatter.stringFromDate(date)
        
        }
        
        cell.nameLabel.text = theDay.title
        cell.dagenLabel.text = date2
        cell.emojiLabel.text = theDay.emojiLabel

        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.destinationViewController.isKindOfClass(ResultViewController) {
            let indexPath = sender as! NSIndexPath
            let resultVC = segue.destinationViewController as! ResultViewController
            resultVC.dagenAftellenInt = indexPath.row
            
        } else if
            segue.destinationViewController.isKindOfClass(AddDayViewController) {
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        self.performSegueWithIdentifier("resultSegue", sender: indexPath)
}

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            

            for notificatie in UIApplication.sharedApplication().scheduledLocalNotifications! {
                let theDay = self.days![indexPath.row]
               // print( "Dit is the day  \(theDay.title)" )
                
                let titel = notificatie.userInfo!["Title"] as! String
               // print ("Dit is userInfo \(titel)")
                
                if titel == theDay.title  {
                    UIApplication.sharedApplication().cancelLocalNotification(notificatie)
                    
                 //   print("gelukt")
                }
            }


            let realm = try! Realm()
            try! realm.write {
                realm.delete(self.days![indexPath.row])
                
                
            }

            self.updateUI()
        }
    }
    
    
}

