//
//  AddDayViewController.swift
//  DagenAftellen
//
//  Created by Arjen Molhoek on 08-03-16.
//  Copyright © 2016 Arjen Molhoek. All rights reserved.
//

import UIKit
import RealmSwift
import pop
import EventKit

class AddDayViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var emojiTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField.delegate = self
        self.emojiTextField.delegate = self
        self.textField.becomeFirstResponder()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddDayViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        self.textField.autocapitalizationType = .Sentences
        
        
        
        
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }

        let newLength = text.utf16.count + string.utf16.count - range.length

        if textField == emojiTextField {

            return newLength <= 3
        } else  {
            return newLength <= 20
        }
       
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()

        return true
    }
    

    
  @IBAction func cancelButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func addDayButtonTapped(sender: AnyObject) {
       
        let reachedDate = self.datePicker.date
        let newDay = DagenAftellen()
        newDay.title = self.textField.text!
        newDay.emojiLabel = self.emojiTextField.text

        newDay.dateReached = reachedDate
        
        if newDay.title != "" {
        let realm = try! Realm()
        try! realm.write {
        realm.add(newDay)
        }
    }
        
        
//        let store = EKEventStore()
//        store.requestAccessToEntityType(.Event) {(granted, error) in
//            if !granted { return }
//            let event = EKEvent(eventStore: store)
//            event.title = self.textField.text!
//            event.startDate = reachedDate
//
//            event.endDate = event.startDate.dateByAddingTimeInterval(2*60*60)
//            event.calendar = store.defaultCalendarForNewEvents
//            do {
//                try store.saveEvent(event, span: .ThisEvent, commit: true)
//            } catch {
//                // Display error to user
//            }
//        }
        
        if newDay.title == "" {
            let alert = UIAlertController(title: "Leeg?", message: "Je moet wel een titel invullen...", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
            }))
                self.presentViewController(alert, animated: true, completion: nil)
        }
        self.navigationController?.popViewControllerAnimated(true)

     
        
         // user did allow notifications
        let cal = NSCalendar.currentCalendar()
        let fireDate : NSDate = cal.dateBySettingHour(07, minute: 35, second: 45, ofDate: newDay.dateReached, options: NSCalendarOptions())!
        let notification = UILocalNotification()
        notification.timeZone = NSTimeZone.defaultTimeZone()
        notification.alertBody = "Vandaag heb je een evenement"
        notification.fireDate = fireDate
        notification.userInfo = [ "Title" : self.textField.text!]

        UIApplication.sharedApplication().scheduleLocalNotification(notification)
        notification.applicationIconBadgeNumber = 1
        
        print(UIApplication.sharedApplication().scheduledLocalNotifications) 
    


        
        
    }

}
